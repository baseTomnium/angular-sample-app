import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get('http://localhost:3000/api/categories');
  }
  getMeals(catName: String) {
    return this.http.get(`http://localhost:3000/api/meals/${catName}`);
  }
}
