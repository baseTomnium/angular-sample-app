import {Component, OnInit} from '@angular/core';
import {HttpService} from './services/http.service';
import {Store} from '@ngrx/store';
import {AppState} from './store/AppState';
import {FetchFail, FetchStart, FetchSuccess} from './store/category/categoryActions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private httpService: HttpService, private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new FetchStart({}));
    this.httpService.getCategories().subscribe(
      (data) => {
        this.store.dispatch(new FetchSuccess(data['categories']));
      },
      (error) => {
        console.log(error);
        this.store.dispatch(new FetchFail(error));
      });
  }
}
