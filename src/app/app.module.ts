import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';

import {AppComponent} from './app.component';
import {FooterComponentComponent} from './components/footer-component/footer-component.component';
import {HeaderComponent} from './components/header/header.component';

import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from './components/main/main.component';
import {MenuComponent} from './components/menu/menu.component';
import {HttpClientModule} from '@angular/common/http';
import {CategoryComponent} from './components/category/category.component';
import {metaReducers, reducers} from './store/AppState';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';

const appRoutes: Routes = [
  {path: '', component: MainComponent},
  {path: 'menu', component: MenuComponent},
  {path: 'menu/:cat', component: CategoryComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponentComponent,
    HeaderComponent,
    MainComponent,
    MenuComponent,
    CategoryComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    BrowserModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      // logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
