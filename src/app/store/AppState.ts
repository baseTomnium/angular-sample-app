import {Categories} from '../models/categories';
import {ActionReducer, ActionReducerMap, MetaReducer} from '@ngrx/store';
import {categoryReducer} from './category/categoryReducer';
import {mealReducer, MealState} from './meal/mealReducer';

export interface AppState {
  readonly categories: Categories;
  readonly meals: MealState;
}

export const reducers: ActionReducerMap<AppState> = {
  categories: categoryReducer,
  meals: mealReducer
};

export function logger(reducer: ActionReducer<AppState>):
  ActionReducer<AppState> {
  return function (state: AppState, action: any): AppState {
    console.log('state', state);
    console.log('action', action);
    return reducer(state, action);
  };
}
export const metaReducers: MetaReducer<AppState>[] = [logger];
