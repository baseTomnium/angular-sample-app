import {Categories} from '../../models/categories';
import {Actions, FETCHING_CATEGORY_FAIL, FETCHING_CATEGORY_START, FETCHING_CATEGORY_SUCCESS} from './categoryActions';

const initialState: Categories = {
  isFetching: false,
  error: {},
  categories: []
};

export function categoryReducer(state: Categories = initialState, action: Actions) {
  switch (action.type) {
    case FETCHING_CATEGORY_START: {
      return {
        ...state,
        isFetching: true
      };
    }
    case FETCHING_CATEGORY_SUCCESS: {
      return {
        ...state,
        isFetching: false,
        categories: action.payload
      };
    }
    case FETCHING_CATEGORY_FAIL: {
      return {
        ...state,
        isFetching: false,
        error: action.payload
      };
    }
    default: return state;
  }
}
