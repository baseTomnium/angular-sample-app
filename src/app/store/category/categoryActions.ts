import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {Categories} from '../../models/categories';
import {Category} from '../../models/category';

export const FETCHING_CATEGORY_START = 'FETCHING_CATEGORY_START';
export const FETCHING_CATEGORY_SUCCESS = 'FETCHING_CATEGORY_SUCCESS';
export const FETCHING_CATEGORY_FAIL = 'FETCHING_CATEGORY_FAIL';


export class FetchStart implements Action {
  readonly type = FETCHING_CATEGORY_START;
  constructor(public payload: Object) {
  }
}

export class FetchSuccess implements Action {
  readonly type = FETCHING_CATEGORY_SUCCESS;
  constructor(public payload: Category[]) {
  }
}

export class FetchFail implements Action {
  readonly type = FETCHING_CATEGORY_FAIL;
  constructor(public payload: Object) {
  }
}

export type Actions = FetchStart | FetchSuccess | FetchFail;
