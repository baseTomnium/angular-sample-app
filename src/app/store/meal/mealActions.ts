import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {Meal} from '../../models/meal';
import {Category} from '../../models/category';

export const FETCHING_MEAL_START = 'FETCHING_MEAL_START';
export const FETCHING_MEAL_SUCCESS = 'FETCHING_MEAL_SUCCESS';
export const FETCHING_MEAL_FAIL = 'FETCHING_MEAL_FAIL';


export class MealFetchStart implements Action {
  readonly type = FETCHING_MEAL_START;

  constructor(public payload: Object) {
  }
}

export class MealFetchSuccess implements Action {
  readonly type = FETCHING_MEAL_SUCCESS;

  constructor(public payload: { 'meals': Meal[], 'category': Category }) {
  }
}

export class MealFetchFail implements Action {
  readonly type = FETCHING_MEAL_FAIL;

  constructor(public payload: Object) {
  }
}

export type Actions = MealFetchStart | MealFetchSuccess | MealFetchFail;
