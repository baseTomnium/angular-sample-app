import {Actions, FETCHING_MEAL_START, FETCHING_MEAL_SUCCESS, FETCHING_MEAL_FAIL} from './mealActions';
import {Category} from '../../models/category';
import {Meal} from '../../models/meal';

export interface MealState {
  isFetching: boolean;
  error: {};
  meals: Meal[];
  category: Category;
}

const initialState: MealState = {
  isFetching: false,
  error: {},
  meals: [],
  category: new Category()
};

export function mealReducer(state = initialState, action: Actions) {
  switch (action.type) {
    case FETCHING_MEAL_START: {
      return {
        ...state,
        isFetching: true
      };
    }
    case FETCHING_MEAL_SUCCESS: {
      return {
        ...state,
        isFetching: false,
        meals: action.payload['meals'],
        category: action.payload['category']
      };
    }
    case FETCHING_MEAL_FAIL: {
      return {
        ...state,
        isFetching: false,
        error: action.payload
      };
    }
    default:
      return state;
  }
}
