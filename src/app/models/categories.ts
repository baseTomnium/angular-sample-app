import {Category} from './category';

export interface Categories {
  isFetching: boolean;
  error: Object;
  categories: Category[];
}
