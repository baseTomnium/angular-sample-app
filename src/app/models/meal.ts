export class Meal {
  id: number;
  short_name: string;
  name: string;
  description: string;
  price_small: 2.55;
  price_large: 5.0;
  small_portion_name: string;
  large_portion_name: string;
}
