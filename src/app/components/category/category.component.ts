import {Component, OnInit} from '@angular/core';
import {Meal} from '../../models/meal';
import {HttpService} from '../../services/http.service';
import {ActivatedRoute} from '@angular/router';
import {Category} from '../../models/category';
import {Store, select} from '@ngrx/store';
import {AppState} from '../../store/AppState';
import {MealFetchStart, MealFetchSuccess, MealFetchFail} from '../../store/meal/mealActions';
import {Observable} from 'rxjs';
import {MealState} from '../../store/meal/mealReducer';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  meals: Meal[] = [];
  catName: String;
  category: Category = new Category();

  constructor(private activateRoute: ActivatedRoute, private httpService: HttpService, private store: Store<AppState>) {
    this.catName = activateRoute.pathFromRoot[1].snapshot.url[1].path;
    this.fetchMeals();
  }

  fetchMeals = () => {
    this.store.dispatch(new MealFetchStart({}));
    this.httpService.getMeals(this.catName).subscribe(
      (data) => {
        this.store.dispatch(new MealFetchSuccess({meals: data['menu_items'], category: data['category'][0]}));
      }, (error) => {
        this.store.dispatch(new MealFetchFail(error));
      });
  };

  ngOnInit() {
    console.log('1', this.meals.length);
    const mealStore: Observable<MealState> = this.store.pipe(select('meals'));
    mealStore.subscribe((data) => {
      this.meals = data['meals'];
      this.category = data['category'];
    });
  }
}
