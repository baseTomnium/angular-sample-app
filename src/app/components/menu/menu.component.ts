import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../services/http.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/AppState';
import {Categories} from '../../models/categories';
import {Observable} from 'rxjs/Observable';
import {Category} from '../../models/category';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [HttpService]
})
export class MenuComponent implements OnInit {
  categoriesList: Observable<Categories>;
  categories: Category[] = [];

  constructor(private httpService: HttpService, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.categoriesList = this.store.select('categories');
    this.categoriesList.subscribe((data) => {
      this.categories = data.categories;
    });
  }

}
