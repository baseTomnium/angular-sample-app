// Get dependencies
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const db = require('./server/db');
let router_categories = require('./server/categories/router_categories');
let router_meals = require('./server/meals/router_meals');
let seed_categories = require('./server/categories/seed_categories');
let seed_meals = require('./server/meals/seed_meals');


const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist/sample-angular')));
app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.static(path.join(__dirname, 'src')));

app.use('/api/', router_categories);
app.use('/api/', router_meals);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/sample-angular/index.html'));
});

// seed_categories();
// seed_meals();

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';

app.listen(port, function () {
  console.log('Server run on port',port);
});
