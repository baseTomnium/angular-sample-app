let express = require('express');
let router = express.Router();
let meals = require('./meals');
let Meals = require('./model_meals');
let Category = require('./../categories/model_category');

router.get('/meals/:shortCategoryName', function(req, res) {
    let shortCategoryName = req.params.shortCategoryName;
    Promise.all([Meals.find({short_name: new RegExp('^'+shortCategoryName, "i")}), Category.find({short_name: new RegExp('^'+shortCategoryName, "i")})])
        .then(([meals, category]) => {
          console.log(meals.length)
            res.json({menu_items : meals, category : category});
        })
        .catch(err =>{
            console.log(err);
            res.json(err);
        })
});


module.exports = router;
