let express = require('express');
let router = express.Router();
let categories = require('./categories');
let Category = require('./model_category');




router.get('/categories/', function(req, res) {
    Category.find({})
        .then(data =>{
            res.json({categories:data});
        })
        .catch(err => {
            console.log(err);
            res.json(err);
        })
});


module.exports = router;
